<?php

namespace App;

use StoutLogic\AcfBuilder\FieldsBuilder;

$config = (object) [
	'ui' => 1,
	'wrapper' => ['width' => 100],
];


$wildgrid = new FieldsBuilder('wildgrid');

$wildgrid
	->addGroup('grid')
		//Cell Large
		->addSelect('cell_size', [
			'label' => 'Width of Cell (Large)',
			'wrapper' => ['width' => 33]
		])
	  	->addChoices(
		  ['2' => '2'],
		  ['3' => '3'],
		  ['4' => '4'],
		  ['5' => '5'],
		  ['6' => '6'],
		  ['7' => '7'],
		  ['8' => '8'],
		  ['9' => '9'],
		  ['10' => '10'],
		  ['11' => '11'],
		  ['12' => '12']
		)
		->setDefaultValue('4')
		->setInstructions('Choose the width of each cell based on a 12 column grid.')

		//Cell Medium
		->addSelect('cell_size_med', [
			'label' => 'Width of Cell (Medium)',
			'wrapper' => ['width' => 33]
		])
	  	->addChoices(
		  ['2' => '2'],
		  ['3' => '3'],
		  ['4' => '4'],
		  ['5' => '5'],
		  ['6' => '6'],
		  ['7' => '7'],
		  ['8' => '8'],
		  ['9' => '9'],
		  ['10' => '10'],
		  ['11' => '11'],
		  ['12' => '12']
		)
		->setDefaultValue('6')
		->setInstructions('Choose the width of each cell based on a 12 column grid.')

		//Cell Small
		->addSelect('cell_size_sm', [
			'label' => 'Width of Cell (Small)',
			'wrapper' => ['width' => 33]
		])
	  	->addChoices(
		  ['2' => '2'],
		  ['3' => '3'],
		  ['4' => '4'],
		  ['5' => '5'],
		  ['6' => '6'],
		  ['7' => '7'],
		  ['8' => '8'],
		  ['9' => '9'],
		  ['10' => '10'],
		  ['11' => '11'],
		  ['12' => '12']
		)
		->setDefaultValue('12')
		->setInstructions('Choose the width of each cell based on a 12 column grid.')
		->endGroup();

return $wildgrid;