<?php

/**
 * Simple function to pretty up our field partial includes.
 *
 * @param  mixed $partial
 * @return mixed
 */
function get_field_partial($partial)
{
    $partial = str_replace('.', '/', $partial);    
    return include(get_stylesheet_directory() ."/app/fields/{$partial}.php");
}
