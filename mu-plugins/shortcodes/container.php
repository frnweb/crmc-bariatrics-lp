<?php
// CONTAINER
	function sl_container ( $atts, $content = null ) {
	    $specs = shortcode_atts( array(
	        'class'     => '',
			), $atts );
		$content = wpautop(trim($content));
	    return '<div class="sl_container ' . esc_attr($specs['class']) . ' ">' .  do_shortcode ( $content ) . '</div>';
	}

	add_shortcode ('container', 'sl_container' );
///CONTAINER
?>