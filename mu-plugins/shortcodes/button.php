<?php
	// BUTTON
		function sl_button ( $atts, $content = null ) {
			$specs = shortcode_atts( array(
				'url'	=> '#',
				'class'	=> '', 
				'target'	=> '_self'
				), $atts );
			if(in_array('read-next', $specs, true)){
				return '<div class="sl_read-next--button"><h4>Read Next:</h4><a href="' . esc_attr($specs['url'] ) . '" class="sl_read-next--link sl_button sl_button--secondary sl_button--' . esc_attr($specs['class'] ) . '" target="'. esc_attr($specs['target'] ) .'">' . $content . '</a></div>';
			} else {
			return '<a href="' . esc_attr($specs['url'] ) . '" class="sl_button sl_button--' . esc_attr($specs['class'] ) . '" target="'. esc_attr($specs['target'] ) .'">' . $content . '</a>';
			};
		}

		add_shortcode ('button', 'sl_button' );
	///BUTTON

	// BUTTON GROUP
		function sl_button_group ( $atts, $content = null ) {
			$specs = shortcode_atts( array(
				'class'	=> '', 
				), $atts );
			return '<div class="sl_button-group sl_button-group--' . esc_attr($specs['class'] ) . '">' . do_shortcode(shortcode_unautop( $content )) . '</div>';
		}

		add_shortcode ('button-group', 'sl_button_group' );
	///BUTTON
?>