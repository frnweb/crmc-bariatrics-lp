<?php
// CALLOUT
	function sl_callout ( $atts, $content = null ) {
		$specs = shortcode_atts( array(
			'class'		=> ''
			), $atts );
		$content = wpautop(trim($content));
		if(in_array('equalizer', $specs, true)){
			$callout = '<div class="sl_callout sl_callout--' . esc_attr($specs['class'] ) . '" data-equalizer-watch>' . do_shortcode ( $content ) . '</div>';
		}else{
			$callout = '<div class="sl_callout sl_callout--' . esc_attr($specs['class'] ) . '">' . do_shortcode ( $content ) . '</div>';
		}
		return $callout;
	}
	add_shortcode ('callout', 'sl_callout' );
///CALLOUT
?>