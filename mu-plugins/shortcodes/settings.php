<?php
// This function reformats autop inside shortcodes. To turn off auto p inside shortcode, wrap shortcode in [shortcode_unautop]

// Example: return '[shortcode_unautop]'. do_shortcode($content) .'[/shortcode_unautop]';

function reformat_auto_p_tags($content) {
    $new_content = '';
    $pattern_full = '{(\[shortcode_unautop\].*?\[/shortcode_unautop\])}is';
    $pattern_contents = '{\[shortcode_unautop\](.*?)\[/shortcode_unautop\]}is';
    $pieces = preg_split($pattern_full, $content, -1, PREG_SPLIT_DELIM_CAPTURE);
    foreach ($pieces as $piece) {
        if (preg_match($pattern_contents, $piece, $matches)) {
            $new_content .= $matches[1];
        } else {
            $new_content .= wptexturize(wpautop($piece));
        }
    }

    return $new_content;
}

// // Allows shortcode use inside ACF fields
function my_acf_add_local_field_groups() {
    remove_filter('acf_the_content', 'wpautop' );
    remove_filter('acf_the_content', 'wptexturize');
}

add_action('acf/init', 'my_acf_add_local_field_groups');
remove_filter('the_content', 'wpautop');
remove_filter('the_content', 'wptexturize');

add_filter('the_content', 'reformat_auto_p_tags', 99);
add_filter('widget_text', 'reformat_auto_p_tags', 99);
add_filter('acf_the_content', 'reformat_auto_p_tags', 99);



?>