=== Slate Admin Theme ===
Contributors: UHS Web Team
Donate Link: 
Tags: wordpress admin theme, admin theme, white label, admin page, wordpress admin panel, admin, plugin, admin panel, wordpress, wordpress admin panel, flat admin theme, modern admin theme, simple admin theme, admin theme style plugin, free admin theme style plugin, backend theme, custom admin theme, new admin ui, wp admin theme, wp admin page.
Requires at least: 4.0
Tested up to: 5.4.2

== Description ==

Plugin to coincide with the Slate Theme from the UHS Web Team.

== Installation ==

1. Unzip `slate-plugin.zip`
2. Upload the `slate-plugin' folder to the `/wp-content/plugins/` directory
3. Activate the plugin through the 'Plugins' menu in WordPress

== Frequently Asked Questions ==

== Screenshots ==

== Changelog ==

== Upgrade Notice ==